import { Component, ViewChild, ElementRef } from '@angular/core';
import { App, NavController, NavParams, ViewController, PopoverController } from 'ionic-angular';
import { TransactionPage } from "../transaction/transaction";
/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'home-popover',
  templateUrl: 'home-popover.html'
})

export class PopoverPage {

  items: any = [
    {name: 'Account', icon: ''}
  ]

  constructor(private navParams: NavParams) {

  }

}


@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  formContainer: boolean = false;
  formTrx = {}

  constructor(
    public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController,
    public popoverCtrl: PopoverController,
    public app: App
    ) {
    let id = navParams.get('id')
  }

  ionViewWillEnter() {
    this.viewCtrl.showBackButton(false);
    console.log(this.formTrx)
  }

  transaction(params) {
    
    this.navCtrl.push(TransactionPage, {
      type: params.name,
      label: params.label
    })
  }
  presentPopover(ev) {

    let popover = this.popoverCtrl.create(PopoverPage);

    popover.present({
      ev: ev
    });
  }

  openTransaksi(data) {
    // this.navCtrl.push(TransactionPage, {
    //   type: data
    // })
    this.app.getRootNav().push(TransactionPage, {
      type: data
    })
  }

  openForm(data: string) {
    this.formContainer = false;
    this.formTrx = {
      pulsa: false,
      kuota: false
    }
    for(let a in this.formTrx ) {
      if(a = data) {
        this.formContainer = true;
        this.formTrx[a] = true
      } else {
        this.formTrx[a] = false
      }
    }
  }

}
