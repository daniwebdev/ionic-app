import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the TrxHistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-trx-history',
  templateUrl: 'trx-history.html',
})
export class TrxHistoryPage {
  public trxs: any = [
    {
      title: 'Indosat Ooridoo Rp. 30.0000',
      tanggal: '12 Okt 2018',
      status: 'Success'
    },
    {
      title: 'Indosat Ooridoo Rp. 50.0000',
      tanggal: '12 Okt 2018',
      status: 'Pending'
    },
    {
      title: 'Indosat Ooridoo Rp. 50.0000',
      tanggal: '12 Okt 2018',
      status: 'Pending'
    },
    {
      title: 'Indosat Ooridoo Rp. 50.0000',
      tanggal: '12 Okt 2018',
      status: 'Pending'
    },
    {
      title: 'Indosat Ooridoo Rp. 50.0000',
      tanggal: '12 Okt 2018',
      status: 'Pending'
    },
    {
      title: 'Indosat Ooridoo Rp. 50.0000',
      tanggal: '12 Okt 2018',
      status: 'Pending'
    },
  ]

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TrxHistoryPage');
  }

  detailTrx(data) {
    alert(data)
  }
}
