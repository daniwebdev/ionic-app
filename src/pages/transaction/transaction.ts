import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the TransactionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-transaction',
  templateUrl: 'transaction.html',
})

export class TransactionPage {
  type: any
  label: any
  items: any = [{name: 'Dani'}, {name: 'Arif'}]

  provide: any = {
    'telkomsel': ['0811','0812','0813','0821','0822','0823','0852','0853','0851'],
    'indosat': ['0814','0815','0816','0855','0856','0857','0858'],
    'xl':['0817','0818','0819','0859','0877','0878'],
    'three': ['0895','0896','0897','0898','0899'],
    'smart': ['0881','0882','0883','0884','0885','0886','0887','0888','0889'],
    'ceria': ['0828'],
    'exis': ['0838','0831','0832','0833']
  }

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.type = navParams.get('type')
    this.label = navParams.get('label')
  }

  ionViewDidLoad() {
    console.log(this.type)
  }

}
