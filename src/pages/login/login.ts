import { Component } from '@angular/core';
import { NavController, LoadingController, NavParams } from 'ionic-angular';

import { TabsPage } from "../tabs/tabs";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})


export class LoginPage {
  
  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public params: NavParams
    ) {
  }
  
  ionViewDidLoad() {
  }

  login() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();  
    setTimeout(() => {
      loading.dismiss();
      this.navCtrl.setRoot(TabsPage, {
        id: "123",
        name: "Carl"
      })
    }, 1000);
  }
}
